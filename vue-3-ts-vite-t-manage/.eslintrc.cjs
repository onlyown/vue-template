module.exports = {
    root: true,
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    globals: {
        defineProps: 'readonly',
        defineEmits: 'readonly',
    },
    extends: [
        'plugin:vue/vue3-essential',
        'plugin:vue/vue3-recommended',
        'plugin:@typescript-eslint/recommended',
        '@vue/standard',
        '@vue/typescript/recommended',
    ],
    overrides: [
        {
            env: {
                node: true,
            },
            files: [
                '.eslintrc.{js,cjs}',
            ],
            parserOptions: {
                sourceType: 'script',
            },
        },
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        type: 'module',
        parser: '@typescript-eslint/parser',
        project: [],
        ecmaFeatures: {
            // Vue 的js或者ts代码中可以写标签 Allows for the parsing of JSX
            jsx: true,
        },
    },
    plugins: [
        'vue',
        '@typescript-eslint',
    ],
    rules: {
        '@typescript-eslint/strict-boolean-expressions': 0,
        '@typescript-eslint/prefer-nullish-coalescing': 0,
        '@typescript-eslint/comma-dangle': 0,
        '@typescript-eslint/indent': 0,
        '@typescript-eslint/semi': 0,
        '@typescript-eslint/object-curly-spacing': 0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
        '@typescript-eslint/no-non-null-assertion': 0,
        '@typescript-eslint/camelcase': 0,
        '@typescript-eslint/no-explicit-any': 0,
        '@typescript-eslint/no-this-alias': 0,
        '@typescript-eslint/no-inferrable-types': 0,
        '@typescript-eslint/no-unused-vars': ['off'],
        '@typescript-eslint/ban-types': 0,
        '@typescript-eslint/eslint-disable-next-line': 0,
        // 关闭名称校验
        '@vue/vue/multi-word-component-names': 'off',
        '@vue/vue/html-closing-bracket-spacing': 0,
        '@vue/vue/max-attributes-per-line': 0,
        // html关闭“>”需要换行
        '@vue/vue/html-closing-bracket-newline': 0,
        'prettier/prettier': 0,
        'no-useless-computed-key': 0,
        'yoda': 0,
        'object-shorthand': 0,
        'vue/html-indent': 0,
        'new-cap': 0,
        'no-useless-constructor': 0,
        'eslint-disable-next-line': 0,
        'eslint-disable': 0,
        quotes: [1, 'single'],
        // 语句强制分号结尾
        semi: [2, 'always'],
        'no-irregular-whitespace': 2,
        'no-case-declarations': 0,
        'no-undef': 0,
        'eol-last': 1,
        'block-scoped-var': 2,
        'comma-dangle': [2, 'only-multiline'],
        'no-dupe-keys': 2,
        'no-empty': 1,
        'no-extra-semi': 2,
        'no-multiple-empty-lines': [1, {
            max: 1,
            maxEOF: 1,
        }],
        'no-trailing-spaces': 1,
        'no-use-before-define': 0,
        'no-unused-vars': ['off'],
        'semi-spacing': [2, {
            before: false,
            after: true,
        }],
        'no-unreachable': 1,
        'space-infix-ops': 1,
        'spaced-comment': 1,
        'no-var': 2,
        'no-multi-spaces': 2,
        'comma-spacing': 1,
        // 缩进风格
        indent: [
            'off',
            4,
            {
                SwitchCase: 1,
                VariableDeclarator: 2,
            },
        ],
        // 关闭名称校验
        'vue/multi-word-component-names': 'off',
        'vue/html-closing-bracket-spacing': 0,
        'vue/max-attributes-per-line': 0,
        // html关闭“>”需要换行
        'vue/html-closing-bracket-newline': 0,
        'vue/first-attribute-linebreak': 0,
        'vue/html-self-closing': 0,
        'vue/singleline-html-element-content-newline': 0,
        // 大括号内是否允许不必要的空格
        'object-curly-spacing': [0, 'never'],
        'padded-blocks': 'off',
        // 函数定义时括号前面要不要有空格
        'space-before-function-paren': [0, 'always'],
    },
};
