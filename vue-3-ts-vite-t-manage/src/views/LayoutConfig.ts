import LayoutMapper from '@/views/layout/LayoutMapper';
import LayoutGroup from '@/views/layout/LayoutGroup';
import {reactive, ref, UnwrapRef, UnwrapNestedRefs} from 'vue';

class LayoutConfig {
    /**
     * light dark
     */
    public themeMode: string = '';
    public themeColor: string = '';
    public layoutViewMapper: UnwrapNestedRefs<LayoutMapper> = reactive<LayoutMapper>(new LayoutMapper());

    public getDisplayMode(): 'dark' | 'light' | '' {
        if (this.themeMode === 'auto') {
            const media = window.matchMedia('(prefers-color-scheme:dark)');
            if (media.matches) {
                return 'dark';
            }
            return 'light';
        }
        return this.themeMode as 'dark' | 'light' | '';
    }

    public setThemeMode(themeMode: string) {
        document.documentElement.setAttribute('theme-mode', themeMode);
    }

    public setThemeColor(themeColor: string) {
        document.documentElement.setAttribute('theme-color', themeColor);
    }

    public initialize() {
        const themeMode = this.getDisplayMode();
        this.setThemeMode(themeMode);
        this.setThemeColor(this.themeColor);
    }
}

export default new LayoutConfig();
