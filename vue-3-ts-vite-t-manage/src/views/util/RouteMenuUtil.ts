import MenuItem from '@/views/layout/component/menu/MenuItem';
import PathUtil from '@/app/com/common/util/PathUtil.ts';

class RouteMenuUtil {

    public static sortMenus(menus: MenuItem[]): void {
        if (menus) {
            menus.sort((a, b) => {
                const as = a.sort ? a.sort : 0;
                const bs = b.sort ? b.sort : 0;
                return as - bs;
            });
        }
    }

    public static convertMenus(routes: any[], superKey: string): MenuItem[] {
        superKey = (superKey) || '';
        const menus: MenuItem[] = [];
        if (routes && routes.length > 0) {
            for (const route of routes) {
                if (route) {
                    let title = '菜单';
                    let icon = '';

                    const meta = route.meta;
                    let auth: boolean = true;
                    let hide: boolean = false;
                    let sort: number = 0;
                    if (meta) {
                        title = meta.title;
                        icon = meta.icon;
                        hide = (meta.hide) ? meta.hide : false;
                        if (typeof meta.sort === 'number') {
                            sort = meta.sort;
                        }
                        if (meta.path) {
                            const path: any = meta.path;
                            if (path && path.has) {
                                auth = path.has();
                            }
                        }
                    } else {
                        auth = false;
                    }
                    if (auth) {
                        const path: string = route.path;
                        // const key = (path.startsWith('/')) ? path : superKey + '/' + path;
                        const key = PathUtil.mergePath(superKey, path);
                        const children = route.children;

                        if (!hide) {
                            const item = new MenuItem();
                            item.text = title;
                            item.icon = icon;
                            item.key = key;
                            item.sort = sort;
                            menus.push(item);
                            if (children && children.length > 0) {
                                const nodes = RouteMenuUtil.convertMenus(children, key);
                                item.children = nodes;
                            }
                        } else {
                            if (children && children.length > 0) {
                                const nodes = RouteMenuUtil.convertMenus(children, key);
                                nodes.forEach((value, index, array) => {
                                    menus.push(value);
                                });
                            }
                        }
                    }
                }
            }
        }
        RouteMenuUtil.sortMenus(menus);
        return menus;
    }
}

export default RouteMenuUtil;
