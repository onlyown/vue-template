export class StyleUtil {

    public static hasClass(element: any, cssClass: any) {
        return !!element.className.match(new RegExp('(\\s|^)' + cssClass + '(\\s|$)'));
    }

    public static addClass(element: any, cssClass: any) {
        if (!StyleUtil.hasClass(element, cssClass)) {
            element.className += ' ' + cssClass;
        }
    }

    public static removeClass(element: any, cssClass: any) {
        if (StyleUtil.hasClass(element, cssClass)) {
            const reg = new RegExp('(\\s|^)' + cssClass + '(\\s|$)');
            element.className = element.className.replace(reg, ' ');
        }
    }
}
