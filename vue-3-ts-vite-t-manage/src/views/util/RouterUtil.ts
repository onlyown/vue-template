import routerManager from '@/router/RouterManager';

const router = routerManager.getRouter();

class RouterUtil {

    public static back() {
        const length = window.history.length;
        const hasHistory = length > 1;
        hasHistory ? router.go(-1) : router.push('/');
    }

    public static getPath(): string {
        const path = router.currentRoute?.value.path;
        return path;
    }

    public static toByName(name: string, query?: any) {
        const route = {
            name,
            query
        };
        router.push(route).then((r) => {
            // no
        });
    }

    public static toByPath(path: string, query?: any) {
        const route = {
            path,
            query
        };
        router.push(route).then((r) => {
            // no
        });
    }
}

export default RouterUtil;
