export default class TagItem {
    public title: string = '';
    public key: string = '';
    private selectedListener: any[] = [];

    private doSelected() {
        for (const c of this.selectedListener) {
            if (typeof c === 'function') {
                c(this);
            }
        }
    }

    public select: () => void = () => {
        this.doSelected();
    };

    public addSelectedListener(listener: (item: TagItem) => void) {
        if (this.selectedListener.indexOf(listener) < 0) {
            this.selectedListener.push(listener);
        }
    }
}
