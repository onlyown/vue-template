import TagItem from '@/views/layout/component/tag/TagItem';

export default class TagViewMapper {

    public items: TagItem[] = [];
    public active: string = '';
    private map: Map<string, TagItem> = new Map<string, TagItem>();

    public setSelectTagKey(key: string) {
        this.active = key;
    }

    public hasTag(key: string): boolean {
        return this.map.has(key);
    }

    public addOrSelectTag(key: string, title: string, selectedListener: (item: TagItem) => void) {
        let data = this.map.get(key);
        if (data) {
            // no
            data.title = title;
        } else {
            data = new TagItem();
            data.key = key;
            data.title = title;
            data.addSelectedListener(selectedListener);
            this.map.set(key, data);
            this.items.push(data);
        }
        this.setSelectTagKey(key);
    }

    public selectTag(item: TagItem) {
        if (item) {
            const own = this;
            if (own.active !== item.key) {
                own.setSelectTagKey(item.key);
                item.select();
            }
        }
    }

    public selectTagByKey(key: string) {
        const data = this.map.get(key);
        if (data) {
            this.selectTag(data);
        }
    }

    private selectNext(index: number) {
        const length = this.items.length;
        if (length > 0) {
            const lastIndex = length - 1;
            let showIndex = index - 1;
            if (showIndex < 0) {
                showIndex = 0;
            } else if (showIndex > lastIndex) {
                showIndex = lastIndex;
            }
            const item = this.items[showIndex];
            this.selectTag(item);
        }
    }

    public removeAllTag() {
        for (const data of this.items) {
            if (data) {
                this.map.delete(data.key);
            }
        }
        this.items = [];
    }

    public removeOtherTagByKey(key: string) {
        const data = this.map.get(key);
        this.removeAllTag();
        if (data) {
            this.map.set(key, data);
            this.items.push(data);
            if (this.active !== key) {
                this.selectTag(data);
            }
        }
    }

    public removeCurrentTagByKey(key: string) {
        const data = this.map.get(key);
        if (data) {
            this.map.delete(key);
            const index = this.items.indexOf(data);
            if (index > -1) {
                this.items.splice(index, 1);
            }
            if (this.active === key) {
                this.selectNext(index);
            }
        }
    }

    public removeBeforeTagByKey(key: string) {
        const data = this.map.get(key);
        if (data) {
            const removeKeys = [];
            const index = this.items.indexOf(data);
            if (index > -1) {
                for (let i = 0; i < index; i++) {
                    const node = this.items[i];
                    if (node) {
                        removeKeys.push(node.key);
                        this.map.delete(node.key);
                    }
                }
                this.items.splice(0, index);
            }
            const active = removeKeys.indexOf(this.active) > -1;
            if (active) {
                this.selectNext(index);
            }
        }
    }

    public removeAfterTagByKey(key: string) {
        const data = this.map.get(key);
        if (data) {
            const removeKeys = [];
            const index = this.items.indexOf(data);
            if (index > -1) {
                const length = this.items.length;
                const start = index + 1;
                for (let i = start; i < length; i++) {
                    const node = this.items[i];
                    if (node) {
                        removeKeys.push(node.key);
                        this.map.delete(node.key);
                    }
                }
                const count = length - start;
                this.items.splice(start, count);
            }
            const active = removeKeys.indexOf(this.active) > -1;
            if (active) {
                this.selectNext(index);
            }
        }
    }
}
