export default class MenuItem {
    public key: string = '';
    public text: string = '';
    public icon: string = '';
    public hidden: boolean = false;
    public tag: boolean = true;
    public sort: number = 0;
    public children: MenuItem[] = [];

    private clickListeners: any[] = [];

    private doClick(e: any) {
        for (const c of this.clickListeners) {
            if (typeof c === 'function') {
                c(this, e);
            }
        }
    }

    public click: (e: any) => void = (e) => {
        this.doClick(e);
    };

    public addClickListener(listener: (item: MenuItem, e: any) => void) {
        if (this.clickListeners.indexOf(listener) < 0) {
            this.clickListeners.push(listener);
        }
    }
}

