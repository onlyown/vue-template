import MenuItem from '@/views/layout/component/menu/MenuItem';

export default class MenuViewMapper {

    public collapsed: boolean = false;
    public fixed: boolean = true;
    public theme = 'light';
    public layout: string = '';
    public logoVisible: boolean = true;
    public logoFullUrl: string = '';
    public logoIconUrl: string = '';
    public bottomFullText: string = '';
    public bottomSimpleText: string = '';

    public expands: string[] = [];
    public active: string = '';
    public menus: MenuItem[] = [];

    public setSelectMenuKey(key: string) {
        this.active = key;
        // this.putExpand(key);
    }

    public putExpand(key: string) {
        if (this.expands.indexOf(key) < 0) {
            this.expands.push(key);
        }
    }
}
