import HeaderMenuItem from '@/views/layout/component/top/HeaderMenuItem.ts';

export default class HeaderIconItem {
    public icon: string = '';
    public title: string = '';
    public key: string = '';
    public menus: HeaderMenuItem[] = [];
    private selectedListener: any[] = [];

    private doSelected(e: Event) {
        for (const c of this.selectedListener) {
            if (typeof c === 'function') {
                c(this, e);
            }
        }
    }

    public select: (e: Event) => void = (e) => {
        this.doSelected(e);
    };

    public addSelectedListener(listener: (item: HeaderIconItem, e: Event) => void) {
        if (this.selectedListener.indexOf(listener) < 0) {
            this.selectedListener.push(listener);
        }
    }
}
