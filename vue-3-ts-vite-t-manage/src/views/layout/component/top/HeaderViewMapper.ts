import HeaderTabItem from '@/views/layout/component/top/HeaderTabItem';
import HeaderIconItem from '@/views/layout/component/top/HeaderIconItem.ts';
import SearchMapper from '@/views/layout/component/search/SearchMapper';
import HeaderUser from '@/views/layout/component/top/HeaderUser.ts';
import HeaderMenuItem from '@/views/layout/component/top/HeaderMenuItem.ts';

export default class HeaderViewMapper {

    public active: string = '';
    public tabs: HeaderTabItem[] = [];
    public icons: HeaderIconItem[] = [];
    public menus: HeaderMenuItem[] = [];
    public searchBar: SearchMapper = new SearchMapper();
    public user: HeaderUser = new HeaderUser();

    public setSelectTabKey(key: string) {
        this.active = key;
    }

    public clearTabs() {
        this.tabs = [];
    }

    public addTab(key: string, title: string, listener: (item: HeaderTabItem) => void) {
        const tab: HeaderTabItem = new HeaderTabItem();
        tab.key = key;
        tab.title = title;
        tab.addSelectedListener(listener);
        this.tabs.push(tab);
    }
}
