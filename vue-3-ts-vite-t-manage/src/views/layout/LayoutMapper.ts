import MenuViewMapper from '@/views/layout/component/menu/MenuViewMapper';
import TagViewMapper from '@/views/layout/component/tag/TagViewMapper';
import LayoutGroup from '@/views/layout/LayoutGroup';
import MenuItem from '@/views/layout/component/menu/MenuItem';
import HeaderViewMapper from '@/views/layout/component/top/HeaderViewMapper';
import SearchData from '@/views/layout/component/search/SearchData';

export default class LayoutMapper {

    public headerView: HeaderViewMapper = new HeaderViewMapper();
    public menuView: MenuViewMapper = new MenuViewMapper();
    public tagView: TagViewMapper = new TagViewMapper();
    public footerText: string = '';
    public breadcrumbVisible: boolean = true;
    // public groups: LayoutGroup[] = [];
    public groupMap: Map<string, LayoutGroup> = new Map<string, LayoutGroup>();
    private menuMap: Map<string, MenuItem> = new Map<string, MenuItem>();
    private menuGroupKeyMap: Map<string, string> = new Map<string, string>();
    private selectedListeners: any[] = [];

    public addSelectedListener(listener: (key: string) => void) {
        if (this.selectedListeners.indexOf(listener) < 0) {
            this.selectedListeners.push(listener);
        }
    }

    private doSelectedEvent(key: string) {
        for (const f of this.selectedListeners) {
            if (typeof f === 'function') {
                f(key);
            }
        }
    }

    private selectGroup(group: LayoutGroup) {
        if (group) {
            this.menuView.menus = group.menus;
        }
    }

    public selectTabKey(key: string) {
        const own = this;
        const headerView = this.headerView;
        if (headerView.active !== key) {
            headerView.setSelectTabKey(key);
            const groupMap = this.groupMap;
            const group = groupMap.get(key);
            if (group) {
                own.selectGroup(group);
            }
        }
    }

    public selectTabByMenuKey(key: string) {
        const own = this;
        const menuGroupKeyMap = this.menuGroupKeyMap;
        const tabKey = menuGroupKeyMap.get(key);
        if (tabKey) {
            own.selectTabKey(tabKey);
        }
    }

    public selectMenuKey(key: string) {
        const menuView = this.menuView;
        menuView.setSelectMenuKey(key);
    }

    public selectTagKey(key: string) {
        const data = this.menuMap.get(key);
        if (data && data.tag) {
            const own = this;
            const tagView = this.tagView;
            tagView.addOrSelectTag(key, data.text, (item) => {
                own.selectMenuKey(item.key);
                own.selectTabByMenuKey(item.key);
                own.doSelectedEvent(item.key);
            });
        }
    }

    public selectKey(key: string) {
        const own = this;
        const menuGroupKeyMap = this.menuGroupKeyMap;
        const tabKey = menuGroupKeyMap.get(key);
        own.selectMenuKey(key);
        own.selectTagKey(key);
        if (tabKey) {
            own.selectTabKey(tabKey);
        }
    }

    public switchMenuCollapse() {
        const menuView = this.menuView;
        menuView.collapsed = !menuView.collapsed;
    }

    private putMenuItem(item: MenuItem) {
        if (item) {
            const own = this;
            this.menuMap.set(item.key, item);
            const hasNodes = (item.children && item.children.length > 0);
            if (hasNodes) {
                for (const node of item.children) {
                    this.putMenuItem(node);
                }
            }
            item.addClickListener((menu, e) => {
                own.selectTagKey(menu.key);
                own.doSelectedEvent(menu.key);
            });
        }
    }

    private putMenuGroupKey(groupKey: string, item: MenuItem) {
        if (item) {
            const menuGroupKeyMap = this.menuGroupKeyMap;
            menuGroupKeyMap.set(item.key, groupKey);

            const hasNodes = (item.children && item.children.length > 0);
            if (hasNodes) {
                for (const node of item.children) {
                    this.putMenuGroupKey(groupKey, node);
                }
            }
        }
    }

    private putSearchData(data: MenuItem, superTitle: string) {
        if (data) {
            const own = this;
            let title = data.text;
            title = (superTitle) ? (superTitle + ' > ' + title) : title;

            const hasNodes = (data.children && data.children.length > 0);
            if (hasNodes) {
                for (const item of data.children) {
                    this.putSearchData(item, title);
                }
            } else {
                const sd: SearchData = new SearchData();
                sd.key = data.key;
                sd.label = title;
                this.headerView.searchBar.items.push(sd);
                sd.addSelectedListener((data) => {
                    own.selectTagKey(data.key);
                    own.doSelectedEvent(data.key);
                });
            }
        }
    }

    public addGroup(group: LayoutGroup) {
        if (group) {

            const own = this;
            const headerView = this.headerView;
            const groupMap = this.groupMap;
            const menuMap = this.menuMap;
            const key = group.key;
            const title = group.title;
            const menus = group.menus;
            groupMap.set(key, group);
            if (menus) {
                for (const menu of menus) {
                    this.putMenuItem(menu);
                    this.putSearchData(menu, '');
                    this.putMenuGroupKey(key, menu);
                }
            }

            headerView.addTab(key, title, (tabItem) => {
                own.selectTabKey(tabItem.key);
            });
        }
    }

    public setGroups(groups: LayoutGroup[]) {
        this.groupMap.clear();
        this.menuMap.clear();
        this.menuGroupKeyMap.clear();
        this.headerView.clearTabs();
        this.headerView.searchBar.items = [];
        if (groups) {
            const own = this;
            for (const group of groups) {
                own.addGroup(group);
            }
            if (groups.length > 0) {
                const group = groups[0];
                const key = group.key;
                this.selectTabKey(key);
            }
        }
    }

    constructor() {
        this.initialize();
    }

    private initialize(): void {
        // no
    }
}
