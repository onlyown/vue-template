import MenuItem from '@/views/layout/component/menu/MenuItem';

export default class LayoutGroup {
    public key: string = '';
    public title: string = '';
    public menus: MenuItem[] = [];
}
