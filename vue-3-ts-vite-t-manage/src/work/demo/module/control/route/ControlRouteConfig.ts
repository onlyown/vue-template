import BaseRouteConfig from '@/router/BaseRouteConfig.ts';

class ControlRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-component';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/avatar',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '头像',
                    icon: 'fa-solid fa-user-tie',
                },
                children: [
                    {
                        path: 'avatar-base',
                        name: 'avatar/avatar-base',
                        component: () => import('@/work/demo/module/control/view/avatar/AvatarBaseView.vue'),
                        meta: {
                            title: '基础头像',
                        },
                    },
                    {
                        path: 'avatar-shape',
                        name: 'avatar/avatar-shape',
                        component: () => import('@/work/demo/module/control/view/avatar/AvatarShapeView.vue'),
                        meta: {
                            title: '头像形状',
                        },
                    },
                    {
                        path: 'avatar-size',
                        name: 'avatar/avatar-size',
                        component: () => import('@/work/demo/module/control/view/avatar/AvatarSizeView.vue'),
                        meta: {
                            title: '头像尺寸',
                        },
                    },
                    {
                        path: 'avatar-type',
                        name: 'avatar/avatar-type',
                        component: () => import('@/work/demo/module/control/view/avatar/AvatarTypeView.vue'),
                        meta: {
                            title: '头像类型',
                        },
                    },
                ],
            },
            {
                path: '/card',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '卡片',
                    icon: 'fa-regular fa-credit-card',
                },
                children: [
                    {
                        path: 'card-base',
                        name: 'card/card-base',
                        component: () => import('@/work/demo/module/control/view/card/CardBaseView.vue'),
                        meta: {
                            title: '基础卡片',
                        },
                    },
                    {
                        path: 'card-footer-data',
                        name: 'card/card-footer-data',
                        component: () => import('@/work/demo/module/control/view/card/CardFooterDataView.vue'),
                        meta: {
                            title: '基础底部展示内容',
                        },
                    },
                ],
            },
            {
                path: '/date',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '日期',
                    icon: 'fa-solid fa-calendar-days',
                },
                children: [{
                    path: 'action-calendar',
                    name: 'list/action-calendar',
                    component: () => import('@/work/demo/module/control/view/date/Calendar.vue'),
                    meta: {
                        title: '日历',
                    },
                }],
            },
            {
                path: '/list',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '列表',
                    icon: 'fa-solid fa-list',
                },
                children: [
                    {
                        path: 'action-list',
                        name: 'list/action-list',
                        component: () => import('@/work/demo/module/control/view/list/ListActionView.vue'),
                        meta: {
                            title: '带操作列表',
                        },
                    },
                ],
            },
            {
                path: '/table',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '表格',
                    icon: 'fa-solid fa-table',
                },
                children: [
                    {
                        path: 'table-base',
                        name: 'table/table-base',
                        component: () => import('@/work/demo/module/control/view/table/TableBaseView.vue'),
                        meta: {
                            title: '基本表格',
                        },
                    },
                    {
                        path: 'table-query',
                        name: 'table/table-query',
                        component: () => import('@/work/demo/module/control/view/table/TableQueryView.vue'),
                        meta: {
                            title: '查询表格',
                        },
                    },
                    {
                        path: 'info-base',
                        name: 'info/table-base',
                        component: () => import('@/work/demo/module/control/hide/HideView.vue'),
                        meta: {
                            hide: true,
                            title: '隐藏信息',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new ControlRouteConfig();
