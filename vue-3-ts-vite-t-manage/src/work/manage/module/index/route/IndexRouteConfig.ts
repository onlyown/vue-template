import BaseRouteConfig from '@/router/BaseRouteConfig.ts';

class IndexRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-manage';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/',
                component: () => import('@/views/Main.vue'),
                meta: {
                    hide: true,
                },
                redirect: '/main/index',
            },
            {
                path: '/main',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '主页',
                    icon: 'fa-solid fa-house',
                    hide: true,
                },
                redirect: '/main/index',
                children: [
                    {
                        path: 'index',
                        name: 'main/index',
                        component: () => import('@/work/manage/module/index/view/Index.vue'),
                        meta: {
                            sort: 0,
                            title: '首页',
                            icon: 'fa-solid fa-house',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new IndexRouteConfig();
