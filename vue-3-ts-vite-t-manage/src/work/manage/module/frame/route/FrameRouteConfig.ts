import BaseRouteConfig from '@/router/BaseRouteConfig.ts';

class FrameRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-manage';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/frame',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '外部页面',
                    icon: 'fa-solid fa-globe',
                },
                children: [
                    {
                        path: 'embed',
                        name: 'frame/embed',
                        component: () => import('@/views/layout/frame/FrameViewPane.vue'),
                        meta: {
                            frameSrc: 'https://tdesign.tencent.com/starter/docs/vue-next/get-started',
                            sort: 0,
                            title: '外部内嵌',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new FrameRouteConfig();
