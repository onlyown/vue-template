import BaseRouteConfig from '@/router/BaseRouteConfig.ts';

class ModuleRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-manage';
    }

    public getRoutes(): any[] {
        const array: any[] = [{
            path: '/system',
            component: () => import('@/views/Main.vue'),
            meta: {
                title: '系统管理',
                icon: 'fa-solid fa-building-user',
            },
            children: [
                {
                    path: 'admin',
                    name: 'system/admin',
                    component: () => import('@/work/manage/module/system/view/AdminListView.vue'),
                    meta: {
                        sort: 0,
                        title: '管理员',
                    },
                },
            ],
        }];
        return array;
    }
}

export default new ModuleRouteConfig();
