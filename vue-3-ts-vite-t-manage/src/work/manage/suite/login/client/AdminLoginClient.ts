import BaseClient from '@/app/lib/client/BaseClient.ts';
class AdminLoginClient extends BaseClient {

    public login(account: string, password: string, back: (data: any) => void) {
        back({info: {success: true}, body: {id: '10000', token: '10000'}});
    }
}

export default new AdminLoginClient();
