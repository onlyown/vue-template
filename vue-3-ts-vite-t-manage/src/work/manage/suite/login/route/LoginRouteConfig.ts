import BaseRouteConfig from '@/router/BaseRouteConfig.ts';

class OperateRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-manage';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/',
                redirect: '/login',
            },
            {
                path: '/login',
                name: 'login',
                component: () => import('@/work/manage/suite/login/LoginView.vue'),
            },
        ];
        return array;
    }

    public getTitle(): string {
        return '系统管理';
    }
}

export default new OperateRouteConfig();
