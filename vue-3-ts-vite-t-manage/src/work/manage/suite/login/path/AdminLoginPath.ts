import Path from '@/com/common/permission/Path.ts';

class AdminLoginPath {
    public service: string = 'manage';
    public login: Path = new Path(this.service, '/api/v1/system/admin/login');
}

export default new AdminLoginPath();
