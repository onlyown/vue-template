import AppContext from '@/app/base/context/AppContext.ts';
import LaunchInitializer from '@/app/base/initialize/LaunchInitializer.ts';
import Auth from '@/com/common/auth/Auth.ts';
import RouterUtil from '@/views/util/RouterUtil.ts';

class LoginStartInitializer extends LaunchInitializer {

    public getOrder(): number {
        return 0;
    }

    public getKey(): string {
        return 'login';
    }

    public initialize(appContext: AppContext): void {
        // no
    }
}

export default new LoginStartInitializer();
