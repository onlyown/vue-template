import EnterInitializer from '@/app/base/initialize/EnterInitializer.ts';
import AppContext from '@/app/base/context/AppContext.ts';
import routeConfigBox from '@/router/RouteConfigBox.ts';
import RouteMenuUtil from '@/views/util/RouteMenuUtil.ts';
import LayoutConfig from '@/views/LayoutConfig.ts';
import RouterUtil from '@/views/util/RouterUtil.ts';

class MainEnterInitializer extends EnterInitializer {

    public getOrder(): number {
        return 100;
    }

    public initialize(appContext: AppContext): void {
        this.initializeData();
        this.initializeView();
    }

    public handleClickOutside() {
        // no
    }

    public initializeView() {
        const own = this;
    }

    public initializeData() {
        this.initializeFunction();
        this.initializePersonal();
    }

    public initializeFunction() {
        // const own = this;
        const own = this;
        const layoutViewMapper = LayoutConfig.layoutViewMapper;

        /** ************ base ***************/
        const logoFullUrl = '/assets/general/image/layout/logo/top-menu-full-logo.png';
        const logoIconUrl = '/assets/general/image/layout/logo/top-menu-icon-logo.png';
        const bottomFullText = '1.0.0';
        const bottomSimpleText = '1.0.0';

        layoutViewMapper.menuView.logoFullUrl = logoFullUrl;
        layoutViewMapper.menuView.logoIconUrl = logoIconUrl;
        layoutViewMapper.menuView.bottomFullText = bottomFullText;
        layoutViewMapper.menuView.bottomSimpleText = bottomSimpleText;
        layoutViewMapper.footerText = '';

        layoutViewMapper.breadcrumbVisible = true;

        layoutViewMapper.headerView.user.icon = '/assets/work/head/user/2.jpg';
        layoutViewMapper.headerView.user.title = '当前用户';

        /** ************ menu ***************/
        const layoutGroups = [];
        layoutGroups.push({
            key: 'system-manage',
            title: '系统管理',
            menus: [],
        });
        layoutGroups.push({
            key: 'system-component',
            title: '组件展示',
            menus: [],
        });

        const tabMap = new Map();
        const menuMap = new Map();

        for (const group of layoutGroups) {
            tabMap.set(group.key, group);
        }
        const configs = routeConfigBox.getRouteConfigs();
        for (const c of configs) {
            const key = c.getKey();
            const menus = RouteMenuUtil.convertMenus(c.getRoutes(), '');
            let ms = menuMap.get(c.getKey());
            if (!ms) {
                ms = [];
                menuMap.set(key, ms);
            }
            for (const m of menus) {
                ms.push(m);
            }
        }
        for (const key of tabMap.keys()) {
            const tab = tabMap.get(key);
            const ms = menuMap.get(key);
            if (ms && ms.length > 0) {
                RouteMenuUtil.sortMenus(ms);
                tab.menus = ms;
            }
        }
        layoutViewMapper.setGroups(layoutGroups);
        layoutViewMapper.addSelectedListener((key) => {
            RouterUtil.toByPath(key);
        });
    }

    public initializeTab() {
        // no
    }

    public initializePersonal() {
        // const own = this;
    }

    public initializeMenu() {
        // no
    }
}

export default new MainEnterInitializer();
