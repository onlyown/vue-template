import httpClient from '@/app/lib/http/HttpClient.ts';
import {AxiosRequestConfig, AxiosResponse} from 'axios';
import routerManager from '@/router/RouterManager.ts';
import config from '@/platform/config/Config.ts';
import Prompt from '@/platform/common/Prompt.ts';
import Info from '@/com/common/message/data/Info.ts';
import auth from '@/com/common/auth/Auth.ts';
import NProgress from 'nprogress';
import LayoutConfig from '@/views/LayoutConfig.ts';
import Logger from '@/app/base/logger/Logger.ts';

import '@/style/app/work.less';
import BaseValueUtil from '@/app/base/util/base/BaseValueUtil.ts';
import LaunchInitializer from '@/app/base/initialize/LaunchInitializer.ts';
import HeaderIconItem from '@/views/layout/component/top/HeaderIconItem.ts';
import I18nManager from '@/i18n/I18nManager.ts';
import {reactive, ref} from 'vue';
import HeaderMenuItem from '@/views/layout/component/top/HeaderMenuItem.ts';
import RouterUtil from '@/views/util/RouterUtil.ts';

class PlatformStartInitializer extends LaunchInitializer {

    public getOrder(): number {
        return 0;
    }

    public getKey(): string {
        return 'platform';
    }

    public initialize(): void {
        this.loadConfig();
        this.initializeView();
        this.initializeApp();
        this.initializeConfig();
    }

    public initializeView() {
        // no
        Logger.info(this, 'initializeView');
        const own = this;
        routerManager.addRouteEnterListener({
            start() {
                NProgress.start();
            },
            end() {
                NProgress.done();
            },
        });

        const layoutViewMapper = LayoutConfig.layoutViewMapper;
        const langList = I18nManager.getLangList();
        let iconItem = reactive(new HeaderIconItem());
        iconItem.title = '切换语言';// this.$t('');
        iconItem.key = 'langSetting';
        iconItem.icon = 'fa-solid fa-language';
        layoutViewMapper.headerView.icons.push(iconItem);
        for (const lang of langList) {

            const menu = new HeaderMenuItem();
            menu.key = lang.value;
            menu.title = (lang.title) ? lang.title : lang.value;
            menu.addSelectedListener((item) => {
                I18nManager.setLang(item.key);
            });
            iconItem.menus.push(menu);
        }

        iconItem = new HeaderIconItem();
        iconItem.title = '系统设置';
        iconItem.key = 'systemSetting';
        iconItem.icon = 'fa-solid fa-gear';
        iconItem.addSelectedListener((item, e) => {
            //
        });
        layoutViewMapper.headerView.icons.push(iconItem);

        const menuItem: HeaderMenuItem = new HeaderMenuItem();
        menuItem.title = '退出';
        menuItem.addSelectedListener((item) => {
            own.logout();
        });

        layoutViewMapper.headerView.menus.push(menuItem);
        // light dark auto
        LayoutConfig.themeMode = 'light';
        LayoutConfig.themeColor = '';
        //
        LayoutConfig.initialize();

        // layoutViewMapper.headerView.menus;
        // fa-solid fa-power-off
        // layoutViewMapper
    }

    public initializeApp() {
        // no
        httpClient.setHttpHandler({
            handleRequest(request: AxiosRequestConfig): void {
                // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
                const head = {
                    token: auth.getToken(),
                    source: auth.getUserId(),
                };
                request.headers = head;
            },
            handleResponse(response: AxiosResponse, back?: (data: any) => void, prompt?: boolean): void {
                // no
                const value = response.data;
                if (typeof (back) === 'function') {
                    back(value);
                }
                const hasBack = (typeof back === 'function');
                if (prompt || !hasBack) {
                    // 需要提示
                    if (!BaseValueUtil.isEmpty(value)) {
                        const info = value.info;
                        if (info && !info.success) {
                            Prompt.message(info, '', '失败！');
                        }
                    }
                }
            },
            handleError(error: any, back?: (data: any) => void, prompt?: boolean): void {
                let message = '网络异常';
                if (error.response) {
                    message = error.message;
                    const response = error.response;
                    const status = response.status;
                    switch (status) {
                        case 400:
                            message = '400！';
                            break;
                        case 401:
                            message = '401！';
                            break;
                        case 403:
                            message = '403！';
                            break;
                        case 404:
                            message = '404！';
                            break;
                        case 500:
                            message = '服务异常！';
                            break;
                        default:
                    }
                } else {
                    const m = error.message;
                    if (m === 'Network Error') {
                        message = '网络错误或者连接超时请稍后重试！';
                    } else {
                        message = m;
                    }
                }
                const hasBack = (typeof back === 'function');
                if (typeof (back) === 'function') {
                    const info = new Info();
                    info.addError('000', message);
                    const data = {info};
                    back(data);
                }
                if (prompt || !hasBack) {
                    Prompt.messageError(message);
                }
            },
            handlePrompt(message: string): void {
                Prompt.messageError(message);
            },
        });
    }

    private initializeConfig() {
        // no
    }

    private loadConfig() {
        // no
        const url = config.getBaseUrl();
        httpClient.setBaseURL(url);

        routerManager.setRouterAuthHandler({
            isAuth(to, form) {
                return {
                    auth: auth.isLogin(),
                    redirect: null,
                };
            },
        });
        routerManager.setDefaultRouteName('login');
        routerManager.setSkipNames(['login']);
        routerManager.setIntercept(true);
    }

    public logout(): void {
        auth.logout();
        RouterUtil.toByPath('/main');
    }
}

export default new PlatformStartInitializer();
