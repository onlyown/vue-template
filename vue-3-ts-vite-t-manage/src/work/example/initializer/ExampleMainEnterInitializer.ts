import EnterInitializer from '@/app/base/initialize/EnterInitializer';
import AppContext from '@/app/base/context/AppContext';
import routeConfigBox from '@/router/RouteConfigBox.ts';
import RouteMenuUtil from '@/views/util/RouteMenuUtil.ts';
import LayoutConfig from '@/views/LayoutConfig.ts';

class ExampleMainEnterInitializer extends EnterInitializer {

    public getOrder(): number {
        return 1010;
    }

    public initialize(appContext: AppContext): void {
        this.initializeData();
        this.initializeView();
    }

    public handleClickOutside() {
        // no
    }

    public initializeView() {
        const own = this;
    }

    public initializeData() {
        this.initializeFunction();
        this.initializePersonal();
    }

    public initializeFunction() {
        // const own = this;
        const own = this;
        const layoutViewMapper = LayoutConfig.layoutViewMapper;

        /** ************ menu ***************/
        const layoutGroups = [];
        layoutGroups.push({
            key: 'system-example',
            title: '示例演示',
            menus: [],
        });

        const tabMap = new Map();
        const menuMap = new Map();

        for (const group of layoutGroups) {
            tabMap.set(group.key, group);
        }
        const configs = routeConfigBox.getRouteConfigs();
        for (const c of configs) {
            const key = c.getKey();
            const menus = RouteMenuUtil.convertMenus(c.getRoutes(), '');
            let ms = menuMap.get(c.getKey());
            if (!ms) {
                ms = [];
                menuMap.set(key, ms);
            }
            for (const m of menus) {
                ms.push(m);
            }
        }
        for (const key of tabMap.keys()) {
            const tab = tabMap.get(key);
            const ms = menuMap.get(key);
            if (ms && ms.length > 0) {
                RouteMenuUtil.sortMenus(ms);
                tab.menus = ms;
            }
        }
        for (const group of layoutGroups) {
            layoutViewMapper.addGroup(group);
        }
    }

    public initializeTab() {
        // no
    }

    public initializePersonal() {
        // const own = this;
    }

    public initializeMenu() {
        // no
    }
}

export default new ExampleMainEnterInitializer();
