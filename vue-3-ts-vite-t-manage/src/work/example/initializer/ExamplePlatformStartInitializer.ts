import LayoutConfig from '@/views/LayoutConfig';
import Logger from '@/app/base/logger/Logger';
import LaunchInitializer from '@/app/base/initialize/LaunchInitializer.ts';

import '@/work/example/style/variables.less';
import '@/work/example/style/index.less';

class ExamplePlatformStartInitializer extends LaunchInitializer {

    public getOrder(): number {
        return 0;
    }

    public getKey(): string {
        return 'platform';
    }

    public initialize(): void {
        this.loadConfig();
        this.initializeView();
        this.initializeApp();
        this.initializeConfig();
    }

    public initializeView() {
        // no
        Logger.info(this, 'initializeView');
        const layoutViewMapper = LayoutConfig.layoutViewMapper;
    }

    public initializeApp() {
        // no
    }

    private initializeConfig() {
        // no
    }

    private loadConfig() {
        // no
    }
}

export default new ExamplePlatformStartInitializer();
