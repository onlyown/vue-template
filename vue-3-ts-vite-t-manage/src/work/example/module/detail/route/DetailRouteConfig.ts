import BaseRouteConfig from '@/router/BaseRouteConfig';

class ResultRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-example';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/detail',
                name: 'detail',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '详情页',
                    icon: 'fa-solid fa-dashboard',
                    sort: 5,
                },
                children: [
                    {
                        path: 'base',
                        name: 'detail/base',
                        component: () => import('@/work/example/module/detail/view/base/index.vue'),
                        meta: {
                            title: '基础详情页',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new ResultRouteConfig();
