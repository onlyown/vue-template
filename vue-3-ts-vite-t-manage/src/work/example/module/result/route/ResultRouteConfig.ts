import BaseRouteConfig from '@/router/BaseRouteConfig';

class ResultRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-example';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                redirect: '/result/success',
                path: '/result',
                name: 'result',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '结果页',
                    icon: 'fa-solid fa-check-circle',
                },
                children: [
                    {
                        path: 'result-403',
                        name: 'result/result-403',
                        component: () => import('@/work/example/module/result/view/Result403Page.vue'),
                        meta: {
                            title: '无权限',
                        },
                    },
                    {
                        path: 'result-404',
                        name: 'result/result-404',
                        component: () => import('@/work/example/module/result/view/Result404Page.vue'),
                        meta: {
                            title: '访问页面不存在页',
                        },
                    },
                    {
                        path: 'result-500',
                        name: 'result/result-500',
                        component: () => import('@/work/example/module/result/view/Result500Page.vue'),
                        meta: {
                            title: '服务器出错页',
                        },
                    },
                    {
                        path: 'browser-incompatible',
                        name: 'result/browser-incompatible',
                        component: () => import('@/work/example/module/result/view/ResultBrowserIncompatiblePage.vue'),
                        meta: {
                            title: '浏览器不兼容页',
                        },
                    },
                    {
                        path: 'fail',
                        name: 'result/fail',
                        component: () => import('@/work/example/module/result/view/ResultFailPage.vue'),
                        meta: {
                            title: '失败页',
                        },
                    },
                    {
                        path: 'maintenance',
                        name: 'result/maintenance',
                        component: () => import('@/work/example/module/result/view/ResultMaintenancePage.vue'),
                        meta: {
                            title: '系统维护页',
                        },
                    },
                    {
                        path: 'network-error',
                        name: 'result/network-error',
                        component: () => import('@/work/example/module/result/view/ResultNetworkErrorPage.vue'),
                        meta: {
                            title: '网络异常',
                        },
                    },
                    {
                        path: 'success',
                        name: 'result/success',
                        component: () => import('@/work/example/module/result/view/ResultSuccessPage.vue'),
                        meta: {
                            title: '项目已创建成功',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new ResultRouteConfig();
