import BaseRouteConfig from '@/router/BaseRouteConfig';

class ResultRouteConfig extends BaseRouteConfig {

    public getKey(): string {
        return 'system-example';
    }

    public getRoutes(): any[] {
        const array: any[] = [
            {
                path: '/table',
                name: 'table',
                component: () => import('@/views/Main.vue'),
                meta: {
                    title: '表格列表',
                    icon: 'fa-solid fa-table',
                },
                children: [
                    {
                        path: 'base-query-table',
                        name: 'table/base-query-table',
                        component: () => import('@/work/example/module/table/view/BaseQueryTable.vue'),
                        meta: {
                            title: '基础查询表格',
                        },
                    },
                ],
            },
        ];
        return array;
    }
}

export default new ResultRouteConfig();
