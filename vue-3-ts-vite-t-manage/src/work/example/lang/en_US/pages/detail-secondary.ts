export default {
    pages: {
        detailSecondary: {
            read: 'Read',
            unread: 'Unread',
            all: 'All',
            setRead: 'set as read',
            setUnread: 'set as unread',
            delete: 'delete',
            empty: 'Empty',
        }
    }
};
