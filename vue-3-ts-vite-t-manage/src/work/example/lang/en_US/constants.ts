export default {
    constants: {
        contract: {
            name: 'Name',
            status: 'Status',
            num: 'Number',
            type: 'Type',
            typePlaceholder: 'Please enter type',
            payType: 'Pay Type',
            amount: 'Amount',
            amountPlaceholder: 'Please enter amount',
            signDate: 'Sign Date',
            effectiveDate: 'Effective Date',
            endDate: 'End Date',
            createDate: 'Create Date',
            attachment: 'Attachment',
            company: 'Company',
            employee: 'Employee',
            pay: 'pay',
            receive: 'received',
            remark: 'remark',
            statusOptions: {
                fail: 'Failure',
                auditPending: 'Pending audit',
                execPending: 'Pending performance',
                executing: 'Successful',
                finish: 'Finish',
            },
            typeOptions: {
                main: 'Master contract',
                sub: 'Subcontract',
                supplement: 'Supplementary contract',
            },
        },
    },
};
