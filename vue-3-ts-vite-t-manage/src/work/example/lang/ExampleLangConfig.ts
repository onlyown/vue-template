import merge from 'lodash/merge';
import BaseLangConfig from '@/i18n/BaseLangConfig.ts';
import BaseLangItem from '@/i18n/BaseLangItem.ts';
import LangImportUtil from '@/i18n/LangImportUtil.ts';

class ExampleLangConfig extends BaseLangConfig {
    public langModules = import.meta.glob('./**/*.ts', {eager: true});

    public getItems(): BaseLangItem[] {
        const own = this;
        // const items: BaseLangItem[] = [];
        // const langModules = own.langModules;
        // const langModuleMap = new Map<string, Object>();
        // if (langModules) {
        //     const fullPaths = Object.keys(own.langModules);
        //
        //     fullPaths.forEach((fullPath) => {
        //         const path = fullPath.replace('./', '');
        //         const module: any = langModules[fullPath];
        //         if (module.default) {
        //
        //             const item = module.default;
        //             const startIndex = 0;
        //             const index = path.indexOf('/');
        //             const code = index > -1 ? path.substring(startIndex, index) : path;
        //
        //             const value: any = langModuleMap.get(code);
        //             if (value) {
        //                 langModuleMap.set(code, merge(value, item));
        //             } else {
        //                 langModuleMap.set(code, item);
        //             }
        //         }
        //     });
        // }
        // langModuleMap.forEach((value: any, key) => {
        //     const item: BaseLangItem = new BaseLangItem();
        //     item.setLang(key);
        //     item.setValue(value);
        //     items.push(item);
        // });
        const items = LangImportUtil.langItems(own.langModules, (fullPath) => {
            const path = fullPath.replace('./', '');
            const startIndex = 0;
            const index = path.indexOf('/');
            const code = index > -1 ? path.substring(startIndex, index) : path;
            return code;
        });
        return items;
    }
}

export default new ExampleLangConfig();
