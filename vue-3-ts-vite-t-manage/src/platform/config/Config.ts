class Config {

    private dev: string = 'http://192.168.3.100:10001';
    private test: string = 'http://192.168.3.100:10001';
    private pro: string = 'http://192.168.3.100:10001';
    private env: string | undefined = process.env.NODE_ENV;

    public setEnv(env: string): void {
        this.env = env;
    }

    public isDevelopment(): boolean {
        return this.env === 'development';
    }

    public isTest(): boolean {
        return this.env === 'test';
    }

    public isProduction(): boolean {
        return this.env === 'production';
    }

    public setDevelopment(): void {
        this.env = 'development';
    }

    public setTest(): void {
        this.env = 'test';
    }

    public setProduction(): void {
        this.env = 'production';
    }

    public getBaseUrl(): string {
        let url = this.pro;
        if (this.env === 'development') {
            url = this.dev;
        } else if (this.env === 'test') {
            url = this.test;
        } else if (this.env === 'production') {
            url = this.pro;
        }
        return url;
    }
}

export default new Config();
