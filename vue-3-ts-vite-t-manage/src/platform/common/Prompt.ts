import {MessagePlugin, NotifyPlugin} from 'tdesign-vue-next';

export default class Prompt {

    public static message(info: any, successText: string, warningText: string) {
        let message = '';
        if (info) {
            if (info.success) {
                message = Prompt.getDefaultPromptText(info);
                if (!message || message === '') {
                    message = successText;
                }
                if (message) {
                    MessagePlugin.success(message);
                }
            } else {
                message = Prompt.getDefaultErrorText(info);
                if (!message || message === '') {
                    message = warningText;
                }
                if (message) {
                    MessagePlugin.error(message);
                }
            }
        } else {
            if (warningText) {
                MessagePlugin.error(warningText);
            }
        }
    }

    public static messageInfo(message: string, title?: string) {
        MessagePlugin.info(message);
    }

    public static messageSuccess(message: string, title?: string) {
        MessagePlugin.success(message);
    }

    public static messageWarning(message: string, title?: string) {
        MessagePlugin.warning(message);
    }

    public static messageError(message: string) {
        MessagePlugin.error(message);
    }

    public static notice(message: string, title?: string, type?: string) {
        type = type || 'info';

        if (type === 'info') {
            title = title || '信息';
            NotifyPlugin.info({
                title,
                content: message,
            });
        }
        if (type === 'success') {
            title = title || '成功';
            NotifyPlugin.success({
                title,
                content: message,
            });
        }
        if (type === 'warn') {
            title = title || '警告';
            NotifyPlugin.warning({
                title,
                content: message,
            });
        }
        if (type === 'error') {
            title = title || '错误';
            NotifyPlugin.error({
                title,
                content: message,
            });
        }
    }

    public static noticeInfo(message: string, title?: string) {
        title = title || '信息';
        NotifyPlugin.info({
            title,
            content: message,
        });
    }

    public static noticeSuccess(message: string, title?: string) {
        title = title || '成功';
        NotifyPlugin.success({
            title,
            content: message,
        });
    }

    public static noticeWarning(message: string, title?: string) {
        title = title || '警告';
        NotifyPlugin.warning({
            title,
            content: message,
        });
    }

    public static noticeError(message: string, title?: string) {
        title = title || '错误';
        NotifyPlugin.error({
            title,
            content: message,
        });
    }

    public static getDefaultErrorText(info: any) {
        let text = '';
        if (info) {
            const warnings = info.prompts;
            const errors = info.errors;
            if (warnings && warnings.length > 0) {
                for (const warning of warnings) {
                    text = text + warning.text + '\n';
                }
            } else if (errors && errors.length > 0) {
                for (const error of errors) {
                    text = text + error.text + '\n';
                }
            }
        }
        return text;
    }

    public static getDefaultPromptText(info: any) {
        let text = '';
        if (info) {
            const prompts = info.prompts;
            if (prompts && prompts.length > 0) {
                for (const prompt of prompts) {
                    text = text + prompt.text + '\n';
                }
            }
        }
        return text;
    }
}
