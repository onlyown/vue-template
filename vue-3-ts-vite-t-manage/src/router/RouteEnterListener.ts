interface RouteEnterListener {

    start(): void;

    end(): void;
}

export default RouteEnterListener;
