import {RouteLocationRaw} from 'vue-router';

export default class RouterAuthInfo {
    public auth: boolean = false;
    public redirect: RouteLocationRaw | null = null;
}
