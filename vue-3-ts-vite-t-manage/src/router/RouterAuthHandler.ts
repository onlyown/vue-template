import {RouteLocationNormalized, RouteLocationRaw} from 'vue-router';
import RouterAuthInfo from '@/router/RouterAuthInfo.ts';

interface RouterAuthHandler {

    isAuth(to: RouteLocationNormalized, from: RouteLocationNormalized): RouterAuthInfo;
}

export default RouterAuthHandler;
