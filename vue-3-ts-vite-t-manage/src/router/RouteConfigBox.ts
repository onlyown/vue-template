import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
import BaseRouteConfig from '@/router/BaseRouteConfig';
import RouteLoader from '@/router/RouteLoader';

class RouteConfigBox {

    public routes: RouteRecordRaw[] = [];
    public configs: BaseRouteConfig[] = [];
    private routeLoader: RouteLoader = new RouteLoader();

    public constructor() {
        this.initialize();
    }

    public add(route: RouteRecordRaw): void {
        this.routes.push(route);
    }

    public getRoutes(): RouteRecordRaw[] {
        return this.routes;
    }

    public getRouteConfigs(): BaseRouteConfig[] {
        return this.configs;
    }

    public addRouteConfig(config: BaseRouteConfig) {
        this.configs.push(config);
        for (const r of config.getRoutes()) {
            this.add(r as RouteRecordRaw);
        }
    }

    private initialize(): void {
        const own = this;
        const array = own.routeLoader.load();
        for (const m of array) {
            this.addRouteConfig(m);
        }
    }
}

export default new RouteConfigBox();
