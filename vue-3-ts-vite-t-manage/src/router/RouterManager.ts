import Vue from 'vue';
import Router, {
    createRouter,
    createWebHistory,
    RouteRecordRaw,
    RouterOptions,
    NavigationGuard,
    RouteLocationNormalized,
    NavigationGuardNext,
    RouteLocationRaw,
} from 'vue-router';
import routeConfigBox from '@/router/RouteConfigBox';
import RouterAuthHandler from '@/router/RouterAuthHandler.ts';
import RouteEnterListener from '@/router/RouteEnterListener';
import RouterAuthInfo from '@/router/RouterAuthInfo.ts';

class RouterManager {

    private router = createRouter({
        history: createWebHistory(import.meta.env.BASE_URL),
        routes: routeConfigBox.getRoutes(),
    });

    private skipNameSet: Set<(string | symbol)> = new Set();
    private skipPathSet: Set<string> = new Set();
    private defaultRouteName: string = '';
    private intercept: boolean = false;
    private routerAuthHandler: RouterAuthHandler = {
        isAuth(to, from) {
            return {
                auth: false,
                redirect: null,
            };
        },
    };

    private enterListeners: RouteEnterListener[] = [];

    public constructor() {
        this.initialize();
    }

    public setRouterAuthHandler(handler: RouterAuthHandler): void {
        this.routerAuthHandler = handler;
    }

    public addSkipName(...skips: (string | symbol)[]): void {
        if (skips) {
            skips.forEach((value, index, array) => {
                this.skipNameSet.add(value);
            });
        }
    }

    public setSkipNames(skips: (string | symbol)[]): void {
        this.skipNameSet = new Set<(string | symbol)>(skips);
    }

    public getSkipNames() {
        return Array.from(this.skipNameSet);
    }

    public addSkipPath(...skips: string []): void {
        if (skips) {
            skips.forEach((value, index, array) => {
                this.skipPathSet.add(value);
            });
        }
    }

    public setSkipPaths(skips: string[]): void {
        this.skipPathSet = new Set<string>(skips);
    }

    public getSkipPaths() {
        return Array.from(this.skipPathSet);
    }

    public setDefaultRouteName(defaultRouteName: string): void {
        this.defaultRouteName = defaultRouteName;
    }

    public setIntercept(intercept: boolean): void {
        this.intercept = intercept;
    }

    public getRouter() {
        return this.router;
    }

    public addRouteEnterListener(listener: RouteEnterListener) {
        if (!this.enterListeners.includes(listener)) {
            this.enterListeners.push(listener);
        }
    }

    private start() {
        for (const l of this.enterListeners) {
            l.start();
        }
    }

    private end() {
        for (const l of this.enterListeners) {
            l.end();
        }
    }

    private initialize() {
        const own = this;
        this.router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext): void => {
            own.start();
            const routerAuthInfo = (own.routerAuthHandler) ? own.routerAuthHandler.isAuth(to, from) : new RouterAuthInfo();
            const auth = routerAuthInfo.auth;
            const redirect = routerAuthInfo.redirect;
            const toName = to.name || '';
            const toPath = to.path;
            const defaultRouteName = own.defaultRouteName;
            let intercept: boolean = own.intercept;
            const skipNameSet = own.skipNameSet;
            const skipPathSet = own.skipPathSet;
            const hasSkipNames = (skipNameSet && skipNameSet.size > 0);
            const hasSkipPaths = (skipPathSet && skipPathSet.size > 0);
            if ((hasSkipNames || hasSkipPaths) && intercept) {
                const skipName = skipNameSet.has(toName);
                const skipPath = skipPathSet.has(toPath);
                if (skipName || skipPath) {
                    intercept = false;
                }
            }
            if (auth) {
                next();
            } else {
                if (redirect) {
                    next(redirect);
                } else if (!intercept) {
                    next();
                } else {
                    const data: Record<string, any> = {
                        to: to,
                        from: from,
                    };
                    const route = {
                        name: defaultRouteName,
                        params: data,
                    };
                    next(route);
                }
            }
            // if (auth || !intercept) {
            //     next();
            // } else {
            //     if (redirect) {
            //         next(redirect);
            //     } else {
            //         const data: Record<string, any> = {
            //             to: to,
            //             from: from,
            //         };
            //         const route = {
            //             name: defaultRouteName,
            //             params: data,
            //         };
            //         next(route);
            //     }
            // }
            // if (toName) {
            //
            // } else {
            //     next({
            //         name: defaultRouteName,
            //     });
            // }
            // own.end();
        });
        this.router.afterEach((to) => {
            own.end();
        });
    }
}

export default new RouterManager();
