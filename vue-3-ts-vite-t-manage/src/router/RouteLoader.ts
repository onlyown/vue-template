import BaseRouteConfig from '@/router/BaseRouteConfig';
// import * as path from 'path';

export default class RouteLoader {

    public load(): BaseRouteConfig[] {
        const own = this;

        const array: BaseRouteConfig[] = [];
        // const root = path.dirname('src');
        const requireContext = import.meta.glob('@/**/*RouteConfig.ts', {eager: true});
        // const requireContext = require.context(
        //     '@/', // 在当前目录下查找
        //     true, // 历子文件夹
        //     /\*?RouteConfig.ts$/, // 正则匹配 以 .ts结尾的文件
        // );
        Object.keys(requireContext).forEach((key) => {
            const component: any = requireContext[key];
            if (component.default) {
                const config = component.default;
                if (config instanceof BaseRouteConfig) {
                    array.push(config);
                }
            }
        });
        return array;
    }
}
