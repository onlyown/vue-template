abstract class BaseRouteConfig {

    public abstract getKey(): string;

    public abstract getRoutes(): any[];
}

export default BaseRouteConfig;
