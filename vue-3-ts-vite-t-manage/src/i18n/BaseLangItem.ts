export default class BaseLangItem {

    private lang = '';
    private value = {};

    public setLang(lang: string) {
        this.lang = lang;
    }

    public getLang(): string {
        return this.lang;
    }

    public setValue(value: {}) {
        this.value = value;
    }

    public getValue(): {} {
        return this.value;
    }
}
