import {createI18n, I18n, useI18n} from 'vue-i18n';
import {useLocalStorage, usePreferredLanguages} from '@vueuse/core';
import LangLoader from '@/i18n/LangLoader.ts';

class I18nManager {
    private langLoader: LangLoader = new LangLoader();
    // public langModules = import.meta.glob('./lang/*/**/*.ts', {eager: true});
    public langMap = this.langLoader.getLangMap();
    public languages = usePreferredLanguages();
    public i18n: I18n;
    public localeConfigKey = 'manage-system-locale';

    public constructor() {
        this.i18n = this.createI18n();
        this.initialize();
    }

    public initialize(): void {
        // no
    }

    // public generateLangModuleMap = () => {
    //     const own = this;
    //     const langModules = own.langModules;
    //     const langModuleMap = new Map<string, Object>();
    //     if (langModules) {
    //         const fullPaths = Object.keys(own.langModules);
    //
    //         fullPaths.forEach((fullPath) => {
    //             const path = fullPath.replace('./lang/', '');
    //             const module: any = langModules[fullPath];
    //             if (module.default) {
    //
    //                 const item = module.default;
    //                 const startIndex = 0;
    //                 const index = path.indexOf('/');
    //                 const code = index > -1 ? path.substring(startIndex, index) : path;
    //
    //                 const value: any = langModuleMap.get(code);
    //                 if (value) {
    //                     langModuleMap.set(code, merge(value, item));
    //                     // Object.keys(item).forEach((name) => {
    //                     //     value[name] = item[name];
    //                     // });
    //                 } else {
    //                     langModuleMap.set(code, item);
    //                 }
    //             }
    //         });
    //     }
    //     Logger.debug(this, 'langModules', own.langModules);
    //     Logger.debug(this, 'langCode', langModuleMap.keys());
    //     return langModuleMap;
    // };

    public getMessage() {
        const message: any = {};
        this.langMap.forEach((value: any, key) => {
            message[key] = value;
        });
        return message;
    }

    public createI18n(): I18n {
        const own = this;
        const i18n = createI18n({
            legacy: false,
            locale: useLocalStorage(own.localeConfigKey, 'zh_CN').value || own.languages.value[0] || 'zh_CN',
            fallbackLocale: 'zh_CN',
            messages: own.getMessage(),
            globalInjection: true,
        });
        return i18n;
    }

    public getI18n(): I18n {
        const own = this;
        return own.i18n;
    }

    public getLangMap() {
        return this.langMap;
    }

    public setLang(lang: string) {
        const own = this;
        if (!own.langMap.has(lang)) {
            lang = 'zh_CN';
        }
        const locale: any = own.i18n.global.locale;
        // const {locale} = useI18n({useScope: 'global'});
        locale.value = lang;
        useLocalStorage(own.localeConfigKey, 'zh_CN').value = lang;
    }

    public getLangList(): { value: string, title: string }[] {
        const list: { value: string, title: string }[] = [];
        const own = this;
        own.langMap.forEach((value: any, key) => {
            list.push({
                title: value.lang,
                value: key,
            });
        });
        return list;
    }
}

const i18nManager = new I18nManager();
export const {t} = i18nManager.i18n.global;
export default i18nManager;
