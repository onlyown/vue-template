import BaseLangItem from '@/i18n/BaseLangItem.ts';
import merge from 'lodash/merge';

export default class LangImportUtil {

    public static langMap<T>(langModules: Record<string, T>, getKey: (path: string) => string) {
        // const own = this;
        const langMap = new Map<string, Object>();
        if (langModules) {
            const fullPaths = Object.keys(langModules);

            fullPaths.forEach((fullPath) => {
                const module: any = langModules[fullPath];
                if (module.default) {

                    const item = module.default;
                    // const startIndex = 0;
                    // const path = fullPath.replace(rootPath, '');
                    // const index = path.indexOf('/');
                    // const code = index > -1 ? path.substring(startIndex, index) : path;
                    const code = getKey(fullPath);

                    const value: any = langMap.get(code);
                    if (value) {
                        langMap.set(code, merge(value, item));
                    } else {
                        langMap.set(code, item);
                    }
                }
            });
        }
        return langMap;
    }

    public static langItems<T>(langModules: Record<string, T>, getKey: (path: string) => string): BaseLangItem[] {
        // const own = this;
        const items: BaseLangItem[] = [];
        const langMap = LangImportUtil.langMap(langModules, getKey);
        if (langMap) {
            langMap.forEach((value: any, key) => {
                const item: BaseLangItem = new BaseLangItem();
                item.setLang(key);
                item.setValue(value);
                items.push(item);
            });
        }
        return items;
    }
}
