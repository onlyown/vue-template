import merge from 'lodash/merge';
import componentsLocale from 'tdesign-vue-next/es/locale/zh_CN';

export default {
    layout: {
        header: {
            search: {
                searchPlaceholder: '输入要搜索内容',
            },
        },
    },
    componentsLocale: merge({}, componentsLocale, {
        // 可以在此处定义更多自定义配置，具体可配置内容参看 API 文档
        // https://tdesign.tencent.com/vue-next/config?tab=api
        // pagination: {
        //   jumpTo: 'xxx'
        // },
    }),
};
