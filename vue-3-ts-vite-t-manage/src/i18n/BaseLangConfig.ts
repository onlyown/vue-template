import BaseLangItem from '@/i18n/BaseLangItem.ts';

export default abstract class BaseLangConfig {

    public abstract getItems(): BaseLangItem[];
}
