import merge from 'lodash/merge';
import BaseLangConfig from '@/i18n/BaseLangConfig.ts';

export default class LangLoader {

    public load(): BaseLangConfig[] {
        const array: BaseLangConfig[] = [];
        const loadRecord = import.meta.glob('@/**/*LangConfig.ts', {eager: true});
        Object.keys(loadRecord).forEach((key) => {
            const component: any = loadRecord[key];
            if (component.default) {
                const config = component.default;
                if (config instanceof BaseLangConfig) {
                    array.push(config);
                }
            }
        });
        return array;
    }

    public getLangMap() {
        const own = this;
        const langMap = new Map<string, Object>();
        const array: BaseLangConfig[] = own.load();
        array.forEach((config) => {
            const items = config.getItems();
            if (items) {
                items.forEach((item) => {
                    const lang = item.getLang();
                    const value = item.getValue();
                    const data: any = langMap.get(lang);
                    if (data) {
                        langMap.set(lang, merge(data, value));
                    } else {
                        langMap.set(lang, value);
                    }
                });
            }
        });
        return langMap;
    }
}
