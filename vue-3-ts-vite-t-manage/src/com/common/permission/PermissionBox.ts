import PathUtil from '@/app/com/common/util/PathUtil';

class PermissionBox {

    private map: Map<string, string> = new Map<string, string>();

    public put(path: string) {
        const key = path;
        if (key) {
            this.map.set(key, '');
        }
    }

    public has(path: string): boolean {
        let has = false;
        const key = path;
        has = this.map.has(key);
        return has;
    }

    public clear(): void {
        this.map.clear();
    }
}

export default new PermissionBox();
