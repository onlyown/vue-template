import PermissionBox from '@/com/common/permission/PermissionBox';
import PathUtil from '@/app/com/common/util/PathUtil.ts';

class Path {

    private readonly paths: string[] = [];
    public path: string = '';

    constructor(...paths: string[]) {
        this.paths = paths;
        this.path = PathUtil.mergePaths(paths);
    }

    public has(): boolean {
        return PermissionBox.has(this.path);
    }

    public getPath(): string {
        return this.path;
    }
}

export default Path;
