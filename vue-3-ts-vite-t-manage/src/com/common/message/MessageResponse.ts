import Info from '@/com/common/message/data/Info.ts';
import MessageRequest from '@/com/common/message/MessageRequest.ts';
import Head from '@/com/common/message/Head.ts';

export default class MessageResponse<H extends Head> extends MessageRequest<H> {

    public static build(action: string): MessageResponse<Head> {
        const head: Head = new Head();
        head.action = action;
        const me: MessageResponse<Head> = new MessageResponse<Head>();
        me.head = head;
        return me;
    }

    public info!: Info;
}
