class InfoUtil {

    public static getDefaultText(info: any) {
        let text = '';
        if (info) {
            const prompts = info.prompts;
            const errors = info.errors;
            if (prompts && prompts.length > 0) {
                for (const prompt of prompts) {
                    text = text + prompt.text + '\n';
                }
            } else if (errors && errors.length > 0) {
                for (const error of errors) {
                    text = text + error.text + '\n';
                }
            }
        }
        return text;
    }

    public static getDefaultErrorText(info: any) {
        let text = '';
        if (info) {
            const errors = info.errors;
            if (errors && errors.length > 0) {
                for (const error of errors) {
                    text = text + error.text + '\n';
                }
            }
        }
        return text;
    }

    public static getDefaultPromptText(info: any) {
        let text = '';
        if (info) {
            const prompts = info.prompts;
            if (prompts && prompts.length > 0) {
                for (const prompt of prompts) {
                    text = text + prompt.text + '\n';
                }
            }
        }
        return text;
    }
}

export default InfoUtil;
