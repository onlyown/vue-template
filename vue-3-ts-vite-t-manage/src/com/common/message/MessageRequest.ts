import Head from '@/com/common/message/Head';

export default class MessageRequest<H extends Head> {

    public static build(action: string): MessageRequest<Head> {
        const head: Head = new Head();
        head.action = action;
        const me: MessageRequest<Head> = new MessageRequest<Head>();
        me.head = head;
        return me;
    }

    public head!: H;
    public body: any;
}
