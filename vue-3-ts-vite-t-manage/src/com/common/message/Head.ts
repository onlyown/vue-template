export default class Head {

    /**
     * 消息的id，标识消息的唯一性
     */
    public key: string = '';
    /**
     * 请求动作类型
     */
    public action: string = '';
    /**
     * 时间（毫秒）
     */
    public timestamp: number = 0;
}
