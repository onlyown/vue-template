import ContentInfo from '@/com/common/message/data/ContentInfo';

export default class Info {
    /**
     * 消息状态：成功：true、失败：false
     */
    public success: boolean = true;
    /**
     * 程序错误信息集合（主要是程序级别错误，如：字段错误、不能为空等）
     */
    public errors: ContentInfo[] = [];
    /**
     * 成功的消息提醒
     */
    public prompts: ContentInfo[] = [];

    public addError(code: string, text: string): void {
        const data: ContentInfo = new ContentInfo();
        data.code = code;
        data.text = text;
        this.errors.push(data);
        this.success = (this.isEmpty(this.errors));
    }

    public addPrompt(code: string, text: string) {
        const data: ContentInfo = new ContentInfo();
        data.code = code;
        data.text = text;
        this.prompts.push(data);
    }

    private isEmpty(list: ContentInfo[]): boolean {
        return (!list) || list.length <= 0;
    }
}
