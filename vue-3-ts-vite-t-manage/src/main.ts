import {createApp} from 'vue';
import TDesign from 'tdesign-vue-next';
import App from './App.vue';
import routerManager from './router/RouterManager';
import store from './store';
import i18nManager from './i18n/I18nManager';

// 引入组件库全局样式资源
import 'tdesign-vue-next/es/style/index.css';

import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css';

import appStartup from '@/app/base/startup/AppStartup'; // progress bar style
// 全局进度条的配置
NProgress.configure({
    easing: 'ease', // 动画方式
    speed: 800, // 递增进度条的速度
    showSpinner: false, // 是否显示加载ico
    trickleSpeed: 200, // 自动递增间隔
    minimum: 0.3, // 更改启动时使用的最小百分比
    parent: 'body', // 指定进度条的父容器
});
appStartup.initialize();

const app = createApp(App);
app.use(store);
app.use(i18nManager.i18n);
app.use(routerManager.getRouter());
app.use(TDesign);
app.mount('#app');
