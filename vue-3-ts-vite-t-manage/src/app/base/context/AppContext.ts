import BaseObjectFactory from '@/app/base/context/BaseObjectFactory.ts';

// type MaterialType<T extends AbstractMaterial> = new(appContext: AppContext) => T;

class AppContext {

    public objectFactory: BaseObjectFactory = new BaseObjectFactory(this);
    private viewTypeMap: Map<any, any> = new Map<any, any>();
    private viewObjectMap: Map<any, any> = new Map<any, any>();

    constructor() {
        this.initialize();
    }

    public getObjectFactory() {
        return this.objectFactory;
    }

    public getObjectByClass<T>(clazz: new (...args: any[]) => T): T {
        return this.objectFactory.getObjectByClass(clazz);
    }

    public getObjectByKey<T>(key: any): T {
        return this.objectFactory.getObjectByKey(key);
    }

    public putObject(key: any, value: any): void {
        this.objectFactory.putObject(key, value);
    }

    public getView<T>(key: any): T {
        let view = this.viewObjectMap.get(key);
        if (!view) {
            const impl = this.viewTypeMap.get(key);
            if (impl) {
                view = this.getObjectByClass(impl);
            }
        }
        return view;
    }

    private initialize(): void {
        // TODO
    }
}

export default AppContext;
