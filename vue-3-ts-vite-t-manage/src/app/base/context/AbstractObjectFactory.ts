import AbstractMaterial from '@/app/base/context/AbstractMaterial';

export default class AbstractObjectFactory extends AbstractMaterial {

    private objectMap: Map<any, any> = new Map<any, any>();

    public putObject(key: any, object: any): void {
        this.objectMap.set(key, object);
    }

    public getObjectByKey(key: any): any {
        return this.objectMap.get(key);
    }

    public getObjectByClass<T>(clazz: new (...args: any[]) => T, createNew: boolean, cover: boolean): T {

        const own = this;
        const map = this.objectMap;
        let o = map.get(clazz);

        if (clazz) {
            const has = map.has(clazz);
            // TODO
            // 不知道同步对象有没有必要写，先注释吧
            if (!has || createNew) {
                o = own.createObjectByClass(clazz);
                // 原来对象不存在，或者覆盖，并且新创建的对象不能为空
                if ((!has || cover) && o) {
                    map.set(clazz, o);
                }
            }
        }
        return o;
    }

    public createObjectByClass<T>(clazz: new (...args: any[]) => T): T {
        let o: any;
        const isMaterial = Object.prototype.isPrototypeOf.call(AbstractMaterial.prototype, clazz.prototype);
        if (isMaterial) {
            o = new clazz(this);
        } else {
            o = new clazz();
        }
        // else if (MaterialUtil.instanceOfMaterial(o)) {
        //     o.setAppContext(this.appContext);
        // }

        if (typeof o.setAppContext === 'function') {
            const setAppContext = o.setAppContext;
            if (setAppContext.length === 1) {
                o.setAppContext(this.appContext);
            }
            // type ParameterType = Parameters<typeof setAppContext> === '';
            // let v: ParameterType;
            //
            // console.log(new v());
        }
        return o;
    }
}
