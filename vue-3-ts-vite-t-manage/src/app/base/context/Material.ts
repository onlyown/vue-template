import AppContext from '@/app/base/context/AppContext';

interface Material {

   // discriminator: 'App-Material';

    setAppContext(appContext: AppContext): void;
}

export default Material;
