import AbstractObjectFactory from '@/app/base/context/AbstractObjectFactory';

export default class BaseObjectFactory extends AbstractObjectFactory {

    private defineClassMap: Map<any, any> = new Map<any, any>();

    public register<P, S extends P>(defineClass: new (...args: any[]) => P, instanceClass: S) {
        this.defineClassMap.set(defineClass, instanceClass);
    }

    public putInstanceClass<T>(key: any, instanceClass: new (...args: any[]) => T) {
        this.defineClassMap.set(key, instanceClass);
    }

    public getObjectByKey(key: any): any {
        const map = this.defineClassMap;
        let o: any;
        const clazz = map.get(key);
        if (clazz) {
            o = super.getObjectByClass(clazz, false, false);
        } else {
            o = super.getObjectByKey(key);
        }
        return o;
    }

    public getObjectByClass<T>(defineClass: new (...args: any[]) => T): T {
        const map = this.defineClassMap;
        let o: any;
        const createNew: boolean = false;
        const cover: boolean = false;
        const clazz = map.get(defineClass);
        if (clazz) {
            o = super.getObjectByClass(clazz, createNew, cover);
        } else {
            o = super.getObjectByClass(defineClass, createNew, cover);
        }
        return o;
    }
}
