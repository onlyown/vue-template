import AppContext from '@/app/base/context/AppContext';

abstract class AbstractMaterial {
    // protected appContext: AppContext;
    public constructor(protected appContext: AppContext) {
        // no
    }
}

export default AbstractMaterial;
