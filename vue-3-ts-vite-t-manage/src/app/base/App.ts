import AppContext from '@/app/base/context/AppContext';
import LaunchInitializerBox from '@/app/base/initialize/LaunchInitializerBox';
import EnterInitializerBox from '@/app/base/initialize/EnterInitializerBox';
import Logger from '@/app/base/logger/Logger';

class App {
    public appContext: AppContext = new AppContext();

    public initializeLaunch(): void {
        Logger.info(this, 'initializeLaunch-start');
        const launchInitializerBox = this.appContext.getObjectByClass(LaunchInitializerBox);
        launchInitializerBox.initialize(this.appContext);
        Logger.info(this, 'initializeLaunch-end');
    }

    public initializeEnter(): void {
        Logger.info(this, 'initializeEnter-start');
        const enterInitializerBox = this.appContext.getObjectByClass(EnterInitializerBox);
        enterInitializerBox.initialize(this.appContext);
        Logger.info(this, 'initializeEnter-end');
    }
}

export default new App();
