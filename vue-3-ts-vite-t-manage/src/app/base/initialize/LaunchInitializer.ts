import AppContext from '@/app/base/context/AppContext';
import BaseInitializer from '@/app/base/initialize/BaseInitializer';

type InitializerType<T extends LaunchInitializer> = new(appContext: AppContext) => T;

export default abstract class LaunchInitializer implements BaseInitializer {

    public abstract initialize(appContext: AppContext): void ;

    public abstract getOrder(): number;

    public abstract getKey(): string;
}
