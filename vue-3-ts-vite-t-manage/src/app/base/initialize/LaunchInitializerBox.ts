import LaunchInitializer from '@/app/base/initialize/LaunchInitializer';
import BaseInitializerBox from '@/app/base/initialize/BaseInitializerBox';

export default class LaunchInitializerBox extends BaseInitializerBox<LaunchInitializer> {

}
