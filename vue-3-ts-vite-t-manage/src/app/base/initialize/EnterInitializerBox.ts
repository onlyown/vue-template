import BaseInitializerBox from '@/app/base/initialize/BaseInitializerBox';
import EnterInitializer from '@/app/base/initialize/EnterInitializer';

export default class EnterInitializerBox extends BaseInitializerBox<EnterInitializer> {

}
