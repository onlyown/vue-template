import BaseInitializer from '@/app/base/initialize/BaseInitializer';
import AppContext from '@/app/base/context/AppContext';

export default abstract class EnterInitializer implements BaseInitializer {

    public abstract initialize(appContext: AppContext): void ;

    public abstract getOrder(): number ;
}
