import AppContext from '@/app/base/context/AppContext';

interface BaseInitializer {

    initialize(appContext: AppContext): void;

    getOrder(): number;
}

export default BaseInitializer;
