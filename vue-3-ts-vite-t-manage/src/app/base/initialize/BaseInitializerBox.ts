import AbstractMaterial from '@/app/base/context/AbstractMaterial';
import BaseInitializer from '@/app/base/initialize/BaseInitializer';
import AppContext from '@/app/base/context/AppContext';

export default abstract class BaseInitializerBox<T extends BaseInitializer> extends AbstractMaterial {

    private map: Map<any, T> = new Map<any, T>();

    public put(data: T): void {
        this.map.set(data, data);
    }

    public initialize(appContext: AppContext) {
        const values: IterableIterator<BaseInitializer> = this.map.values();

        const array: BaseInitializer[] = [];
        for (const data of values) {
            array.push(data);
        }

        array.sort((a, b) => {
            let order1 = a.getOrder();
            let order2 = b.getOrder();
            if (!order1) {
                order1 = 0;
            }
            if (!order2) {
                order2 = 0;
            }
            return order1 - order2;
        });

        for (const v of array) {
            v.initialize(appContext);
        }
    }

    public clear() {
        this.map.clear();
    }
}
