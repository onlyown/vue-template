import StringBaseUtil from '@/app/base/util/base/StringBaseUtil';

export default class BaseValueUtil {

    public static stringTrim(text: string): string {
        const exp = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        return text === null ? '' : (text + '').replace(exp, '');
    }

    public static isEmpty(value: any): boolean {
        let empty = false;
        if (value instanceof Array) {
            empty = value.length <= 0;
        } else {
            empty = BaseValueUtil.stringTrim(value) === '' || value === undefined || value === null || value === 'undefined' || value === 'null' || value === '&nbsp;';
        }
        return empty;
    }

    public static isNotEmpty(value: any): boolean {
        return !BaseValueUtil.isEmpty(value);
    }
}
