import BaseValueUtil from '@/app/base/util/base/BaseValueUtil';

export default class StringBaseUtil {
    public static trim(text: string): string {
        return BaseValueUtil.stringTrim(text);
    }
}
