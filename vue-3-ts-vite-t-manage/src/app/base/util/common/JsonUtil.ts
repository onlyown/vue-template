import BaseValueUtil from '@/app/base/util/base/BaseValueUtil';

export default class JsonUtil {
    /**
     * 对象转成json字符串
     * @param {type} value
     * @returns {String}
     */
    public static toJson(value: object): string {
        if (BaseValueUtil.isEmpty(value)) {
            return '';
        }
        const json = JSON.stringify(value);
        return json;
    }

    /**
     * 将json字符串转成json对象
     * @param {type} json
     * @returns {undefined|Function}
     */
    public static toObject<T>(json: string): T {
        let value;
        if (!BaseValueUtil.isEmpty(json)) {
            try {
                value = (new Function('return ' + json))();
            } catch (e) {
                // do something
            }
        }
        return value;
    }
}
