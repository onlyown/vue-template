import LaunchInitializer from '@/app/base/initialize/LaunchInitializer';
import EnterInitializer from '@/app/base/initialize/EnterInitializer';
import AppContext from '@/app/base/context/AppContext';
import LaunchInitializerBox from '@/app/base/initialize/LaunchInitializerBox';
import EnterInitializerBox from '@/app/base/initialize/EnterInitializerBox';

export default class InitializerLoader {

    public loadInitializers(appContext: AppContext) {
        const own = this;
        const requireContext = import.meta.glob('@/**/*Initializer.ts', {eager: true});
        // const requireContext = require.context(
        //     '@/', // 在当前目录下查找
        //     true, // 历子文件夹
        //     /\*?Initializer.ts$/, // 正则匹配 以 .ts结尾的文件
        // );
        Object.keys(requireContext).forEach((key) => {
            const component: any = requireContext[key];
            if (component.default) {
                const initializer = component.default;
                own.put(initializer, appContext);
            }
        });
    }

    public put(initializer: any, appContext: AppContext): void {

        const launchInitializerBox: LaunchInitializerBox = appContext.getObjectByClass(LaunchInitializerBox);
        const enterInitializerBox: EnterInitializerBox = appContext.getObjectByClass(EnterInitializerBox);
        // initializer.name;
        // const name = fileName.replace(/^\.\/(.*)\.\w+$/, '$1');
        // const initializer = component.default;
        if (initializer instanceof LaunchInitializer) {
            launchInitializerBox.put(initializer);
        }
        if (initializer instanceof EnterInitializer) {
            enterInitializerBox.put(initializer);
        }
    }
}

