import InitializerLoader from '@/app/base/startup/initializer/InitializerLoader';
import app from '@/app/base/App';

class AppStartup {
    private initializerLoader: InitializerLoader = new InitializerLoader();

    public initialize(): void {
        const own = this;
        own.initializerLoader.loadInitializers(app.appContext);
        own.launch();
    }

    private launch(): void {
        app.initializeLaunch();
    }
}

export default new AppStartup();
