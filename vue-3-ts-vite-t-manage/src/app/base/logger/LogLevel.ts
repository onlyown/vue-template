enum LogLevel {
    all,
    trace,
    debug,
    info,
    warn,
    error,
    fatal,
    off,
}

export default LogLevel;
