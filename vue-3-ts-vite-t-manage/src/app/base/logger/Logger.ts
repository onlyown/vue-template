import LogLevel from '@/app/base/logger/LogLevel';

export default class Logger {

    public static level: LogLevel = LogLevel.info;

    public static trace(object: any, message: string): void {
        if (Logger.level <= LogLevel.trace) {
            const data = {
                type: 'trace',
                object,
                message,
            };
            console.log(data);
        }
    }

    public static debug(object: any, message: string, info?: any): void {
        if (Logger.level <= LogLevel.debug) {
            const data = {
                type: 'debug',
                object,
                message,
                info,
            };
            console.log(data);
        }
    }

    public static info(object: any, message: string): void {
        if (Logger.level <= LogLevel.info) {
            // 以下两句是等效的
            // search.caller.name;
            // arguments.callee.caller.name
            const data = {
                type: 'info',
                object,
                message,
            };
            console.log(data);
        }
    }

    public static warn(object: any, message: string): void {
        if (Logger.level <= LogLevel.warn) {
            const data = {
                type: 'warn',
                object,
                message,
            };
            console.log(data);
        }
    }

    public static error(object: any, message: string, error: any): void {
        if (Logger.level <= LogLevel.error) {
            const data = {
                type: 'error',
                object,
                message,
                error,
            };
            console.log(data);
        }
    }

    public static fatal(object: any, message: string, error: any): void {
        if (Logger.level <= LogLevel.fatal) {
            const data = {
                type: 'fatal',
                object,
                message,
                error,
            };
            console.log(data);
        }
    }
}
