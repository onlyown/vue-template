export default class PathUtil {

    public static check(path: string): string {
        if (path) {
            const regex = /\/+/g;
            const gap = '/';
            path = path.replace(regex, gap);
        }
        return path;
    }

    public static mergePath(...paths: string[]): string {
        let path: string = '';
        if (paths) {
            path = '/' + paths.join('/');
        }
        return PathUtil.check(path);
    }

    public static mergePaths(paths: string[]): string {
        let path: string = '';
        if (paths) {
            path = '/' + paths.join('/');
        }
        return PathUtil.check(path);
    }

    public static prependLeadingSlash(path: string): string {
        const pre = '/';
        if (path && !path.startsWith(pre)) {
            return pre + path;
        } else {
            return path;
        }
    }
}
