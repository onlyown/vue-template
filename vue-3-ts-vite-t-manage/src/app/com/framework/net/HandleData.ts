import DataBackAction from '@/app/com/framework/net/DataBackAction';

export default class HandleData {
    public data: any;
    public back?: DataBackAction;
    public parallel: boolean = false;
    public sendTimestamp: number = 0;
}
