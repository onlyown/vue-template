interface InvokeAction {

    invoke(key: string, data: any): void;
}

export default InvokeAction;
