 interface ErrorPrompt {
    prompt(message: string): void;
}
export default ErrorPrompt;
