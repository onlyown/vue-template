 interface ConnectBack {

    onOpen(): void;

    onError(): void;
}
export default ConnectBack;
