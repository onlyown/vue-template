import httpClient from '@/app/lib/http/HttpClient';

export default class BaseClient {

    public post(url: string, data: object, back?: (data: any) => void, prompt?: boolean): void {
        httpClient.post(url, data, back, prompt);
    }
}
