import {defineConfig} from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import svgLoader from 'vite-svg-loader';
import {resolve} from 'path';

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            // 路径别名
            '@': resolve(__dirname, 'src'),
        },
        // 使用路径别名时想要省略的后缀名，可以自己 增减
        // extensions: ['.js', '.json', '.ts'],
    },
    plugins: [
        vue(),
        vueJsx(),
        svgLoader(),
    ],
    define: {
        'process.env': {
            BASE_URL: '',
        },
    },
});
